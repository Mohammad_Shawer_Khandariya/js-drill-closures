function cacheFunction(cb) {
    let cache = {};
    return((val) => {
        if (cache[val] === undefined) {
            cache[val] = cb(val);
        } else {
            return(cache[val]);
        }
    });
}

module.exports = {
    cacheFunction
};