function counterFactory(val) {
    let counter = val;
    return({
        increment: () => {
            return(counter += 1);
        },
        decrement: () => {
            return(counter -= 1);
        }
    })
}

module.exports = {
    counterFactory
}