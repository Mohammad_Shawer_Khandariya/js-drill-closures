function limitFunctionCallCount(cb, n) {
    if(n <= 0 || n == undefined) {
        return(() => null);
    } else {
        let nCopy = n;
        return(() => {
            if (nCopy > 0) {
                cb();
                nCopy -= 1   
            }
        })
    }
}

module.exports = {
    limitFunctionCallCount
};