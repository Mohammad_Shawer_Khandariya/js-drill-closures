let num = 5;
const getFun = require('../root/counterFactory')
const incDec = getFun.counterFactory(num);

if (incDec.increment() === (num += 1) && incDec.decrement() === (num -= 1)) {
    console.log('Test Passed!');
} else {
    console.log('Test Failed!')
}