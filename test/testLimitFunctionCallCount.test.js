const getFun = require('../root/limitFunctionCallCount')
let val = 5;
let num = 0;
const repeatingFunction = getFun.limitFunctionCallCount(showOutput, val);

function showOutput() {
    return(num += 1);
}

repeatingFunction();
repeatingFunction();
repeatingFunction();
repeatingFunction();
repeatingFunction();
repeatingFunction();
repeatingFunction();

if (num === val) {
    console.log('Test Passed!');
} else {
    console.log('Test Failed!')
}