const getFun = require('../root/cacheFunction')
const repeatingFunction = getFun.cacheFunction(logArgument);

let number = 5;
const testData = logArgument(number)

function logArgument(input) {
    return(input * 2);
}

repeatingFunction(1);
repeatingFunction(2);
repeatingFunction(5);
repeatingFunction(3);
repeatingFunction(4);

let outputData = repeatingFunction(number);

if (JSON.stringify(testData) === JSON.stringify(outputData)) {
    console.log('Test Passed!');
} else {
    console.log('Test Failed!')
}